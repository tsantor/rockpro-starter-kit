# RockPro Starter Kit

## Overview
A collection of useful scripts to do a handful of common tasks when setting up a new RockPro. These scripts have been run on "RockPro64 (18.04.1 LTS (Bionic Beaver)".

## Scripts
- Bootstrap
- Install Node 8.x
- Install Python 3.6.x
- Install Phidget Libraries
- Other Misc. Scripts


## Terminal Access
The below username and password are the default upon first booting up.

- **SSH:** ssh rock64@<IP_ADDRESS>
- **Username:** rock64
- **Password:** rock64

## Set Static IP
Edit `/etc/network/interfaces`:

```
auto eth0
iface eth0 inet static
address 192.168.0.101
netmask 255.255.255.0
gateway 192.168.0.1
```
