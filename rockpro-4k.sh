#!/usr/bin/env bash

sudo add-apt-repository ppa:ayufan/rock64-ppa
sudo apt-get update

sudo apt-get install -y ffmpeg mpv libmali-rk-midgard-t86x-r14p0-gbm

# Ayufan has some old documentation
# sudo apt-get install -y libmali-rk-utgard-450-r7p0 linux-rock64-package
# sudo apt-get install -y linux-image-4.4.126-rockchip-ayufan-239

apt-cache policy ffmpeg mpv


# uname -r
# uname -a
# lsb_release -a


wget http://distribution.bbb3d.renderfarming.net/video/mp4/bbb_sunflower_2160p_30fps_normal.mp4
wget http://distribution.bbb3d.renderfarming.net/video/mp4/bbb_sunflower_2160p_60fps_normal.mp4
wget http://distribution.bbb3d.renderfarming.net/video/mp4/bbb_sunflower_1080p_30fps_normal.mp4
wget http://distribution.bbb3d.renderfarming.net/video/mp4/bbb_sunflower_1080p_60fps_normal.mp4
