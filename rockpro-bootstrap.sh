#!/usr/bin/env bash

sudo apt-get update
sudo apt-get -y upgrade

sudo apt-get -y install bash
sudo apt-get -y install build-essential
sudo apt-get -y install cmake
sudo apt-get -y install coreutils
sudo apt-get -y install curl
sudo apt-get -y install git
sudo apt-get -y install grep
sudo apt-get -y install htop
sudo apt-get -y install nano
sudo apt-get -y install nmap
sudo apt-get -y install openssh-server
sudo apt-get -y install openssl
sudo apt-get -y install rsync
sudo apt-get -y install wget
sudo apt-get -y install whois

# Git
git config --global user.name "Tim Santor"
git config --global user.email "tsantor@xstudiosinc.com"


# Make python point to latest python
sudo ln -s /usr/bin/python3.6 /usr/bin/python
